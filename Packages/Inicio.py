from PyQt5 import QtCore, QtGui, QtWidgets

class Inicio(object):
    def __init__(self):
        self.Font = QtGui.QFont()
        self.Font.setFamily("Lato")

    def IniPrincipal(self, MaiWin, App):
        MaiWin.setObjectName("MaiWin")
        MaiWin.setWindowIcon(QtGui.QIcon('Images/10020200613.svg'))

        Screen = App.desktop().screenGeometry()
        ScreenWidth, ScreenHeight = Screen.width(), Screen.height()

        Width, Height = 390, 290

        MaiWin.move(((ScreenWidth / 2) - (Width / 2)), ((ScreenHeight / 2) - (Height / 2) - 100))
        MaiWin.resize(Width, Height)

        MaiWin.setMinimumSize(QtCore.QSize(Width, Height))
        MaiWin.setMaximumSize(QtCore.QSize(Width, Height))

        QSS = open('QSS/StyleSheet.qss')
        MaiWin.setStyleSheet(QSS.read())

        self.IniWid = QtWidgets.QWidget(MaiWin)
        self.IniWid.setObjectName("IniWid")

        self.Font.setPointSize(19)
        self.Font.setBold(True)

        self.Titulo = QtWidgets.QLabel(self.IniWid)
        self.Titulo.setObjectName("Titulo")
        self.Titulo.setGeometry(QtCore.QRect(10, 13, 370, 60))
        self.Titulo.setAlignment(QtCore.Qt.AlignCenter)
        self.Titulo.setFont(self.Font)

        self.Font.setPointSize(12)
        self.Font.setBold(False)

        self.Frame = QtWidgets.QFrame(self.IniWid)
        self.Frame.setObjectName("Frame")
        self.Frame.setGeometry(QtCore.QRect(45, 90, 300, 33))  # x, y, w, h
        self.Frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.Frame.setFrameShadow(QtWidgets.QFrame.Raised)

        self.UsuLinEdi = QtWidgets.QLineEdit(self.Frame)
        self.UsuLinEdi.setObjectName("UsuLinEdi")
        self.UsuLinEdi.setPlaceholderText('Usuario')
        self.UsuLinEdi.setGeometry(QtCore.QRect(3, 3, 294, 27))
        self.UsuLinEdi.setFont(self.Font)
        self.UsuLinEdi.setAlignment(QtCore.Qt.AlignCenter)
        self.UsuLinEdi.setFrame(False)

        self.ImaUsu = QtWidgets.QLabel(self.Frame)
        self.ImaUsu.setObjectName("UsuIma")

        self.Usu = QtGui.QPixmap("Images/Usuario.png").scaled(25, 25, QtCore.Qt.KeepAspectRatio,
                                                              QtCore.Qt.SmoothTransformation)

        self.ImaUsu.setPixmap(self.Usu)
        self.ImaUsu.setAlignment(QtCore.Qt.AlignCenter)
        self.ImaUsu.setGeometry(QtCore.QRect(4, 4, 25, 25))
        self.ImaUsu.raise_()

        self.Frame = QtWidgets.QFrame(self.IniWid)
        self.Frame.setObjectName("Frame")
        self.Frame.setGeometry(QtCore.QRect(45, 139, 300, 33))  # x, y, w, h
        self.Frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.Frame.setFrameShadow(QtWidgets.QFrame.Raised)

        self.ConLinEdi = QtWidgets.QLineEdit(self.Frame)
        self.ConLinEdi.setObjectName("ConLinEdi")
        self.ConLinEdi.setPlaceholderText('Contraseña')
        self.ConLinEdi.setEchoMode(QtWidgets.QLineEdit.Password)
        self.ConLinEdi.setGeometry(QtCore.QRect(3, 3, 294, 27))
        self.ConLinEdi.setFont(self.Font)
        self.ConLinEdi.setAlignment(QtCore.Qt.AlignCenter)
        self.ConLinEdi.setFrame(False)

        self.ImaCon = QtWidgets.QLabel(self.Frame)
        self.ImaCon.setObjectName("ImaCon")

        self.Usu = QtGui.QPixmap("Images/Candado.png").scaled(25, 25, QtCore.Qt.KeepAspectRatio,
                                                              QtCore.Qt.SmoothTransformation)

        self.ImaCon.setPixmap(self.Usu)
        self.ImaCon.setAlignment(QtCore.Qt.AlignCenter)
        self.ImaCon.setGeometry(QtCore.QRect(4, 4, 25, 25))
        self.ImaCon.raise_()

        self.Font.setBold(True)

        self.InicioPusBut = QtWidgets.QPushButton(self.IniWid)
        self.InicioPusBut.setObjectName("InicioPusBut")
        self.InicioPusBut.setGeometry(QtCore.QRect(120, 210, 160, 30))
        self.InicioPusBut.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.InicioPusBut.setFont(self.Font)

        MaiWin.setCentralWidget(self.IniWid)
        self.InicioDatos(MaiWin)
        QtCore.QMetaObject.connectSlotsByName(MaiWin)

    def InicioDatos(self, MaiWin):
        _translate = QtCore.QCoreApplication.translate
        MaiWin.setWindowTitle(_translate("InicioMaiWin", "Inicio de Sesión"))
        self.Titulo.setText("Inicio de Sesión")
        self.InicioPusBut.setText("Inicio")
























